import { createApp } from 'vue'
import { createRouter, createWebHashHistory } from 'vue-router'
import './style.css'

import vant, { Locale } from 'vant'
import enUS from 'vant/es/locale/lang/en-US';
import App from './App.vue'
import 'vant/lib/index.css';
import auth from './auth'
import routes from './router/router'

import { showNotify } from 'vant';
import Vue3ColorPicker from "vue3-colorpicker";
import "vue3-colorpicker/style.css";
const router = createRouter({
  // 4. Provide the history implementation to use. We are using the hash history for simplicity here.
  history: createWebHashHistory(),
  routes, // short for `routes: routes`
})

Locale.use('en-US', enUS)
createApp(App)
  .use(vant)
  .use(router)
  .use(Vue3ColorPicker)
  .mount('#app')


import { reactive } from "vue";
import { io } from "socket.io-client";
import auth from './auth'

export const state = reactive({
  connected: false,
  authMessage: null,
  peerId: null,
  info: {
    type: null,
    action: null
  },
});

// "undefined" means the URL will be computed from the `window.location` object
const URL = undefined;

let socket

auth.login().then(peer => {
  state.peerId = peer.toString()
  socket = io(URL, {
    auth: {
      key: peer.toString()
    }
  });
  socket.on("connect", () => {
    state.connected = true;
  });
  socket.on('error', function(){
    state.connected = false
  });
  socket.on('connect_error', function(err){
    state.authMessage = err.message
    state.connected = false
  });
  socket.on("disconnect", () => {
    state.connected = false;
  });
  
  socket.on("bunker/info", (val) => {
    console.log(val)
    state.info = val
  });
})

export { socket }


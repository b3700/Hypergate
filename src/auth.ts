import { IDBDatastore } from 'datastore-idb'
import { Key } from 'interface-datastore'
import { exportToProtobuf, createFromPrivKey, createFromProtobuf, createEd25519PeerId } from '@libp2p/peer-id-factory'

export default {
  async login() {
    // Init datastore
    const datastore = new IDBDatastore('/keybox')
    await datastore.open()

    let peerId = null
    const keyIndex = new Key('mykey')
    const hasKey = await datastore.has(keyIndex)

    if (hasKey) {
      const key = await datastore.get(keyIndex)
      peerId = await createFromProtobuf(key)
    } else {
      peerId = await createEd25519PeerId()
      const key = exportToProtobuf(peerId)
      await datastore.put(keyIndex, key)
    }
    await datastore.close()

    console.log(peerId)
    return peerId
  }
}
